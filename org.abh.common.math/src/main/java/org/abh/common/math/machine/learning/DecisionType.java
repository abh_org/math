package org.abh.common.math.machine.learning;

public enum DecisionType {
	NUMERICAL,
	TEXT
}
